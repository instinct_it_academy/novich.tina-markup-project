'use strict';

 window.onload = function() {
    var excerptCutElement = document.getElementsByClassName('excerpt-cut-js');

    for(var i = 0; i < excerptCutElement.length; i++) {
        if (excerptCutElement[i].innerHTML.length) {
            excerptCutElement[i].innerHTML = cutString(excerptCutElement[i].innerHTML);
        }
    }
};

function cutString(string) {

    if (typeof string === 'string') {
        return string.substr(0) + ' ...';
    }
    return string;
}


